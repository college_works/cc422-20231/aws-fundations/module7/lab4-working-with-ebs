#!/bin/bash
#
# This script was written to automate the following subtask
# - "Task 1: Create a New EBS Volume"
# of Module 7 - Storage (AWS Cloud Foundations)
#
# Requirements:
# - AWS CLI
# - jq (JSON processor)

INSTANCE_NAME=Lab

describeInstance=$(aws ec2 describe-instances \
                    --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                              "Name=instance-state-name,Values=running" \
                    --query 'Reservations[].Instances[]' \
                    --output json \
                    | jq '.[]')

instanceId=$(echo $describeInstance | jq -r '.InstanceId')
echo "[*] Describing $INSTANCE_NAME instance (InstanceId: $instanceId)"


az=$(echo $describeInstance | jq -r '.Placement.AvailabilityZone')
echo "AvailabilityZone: $az"

volumeId=$(echo $describeInstance | jq -r '.BlockDeviceMappings[].Ebs.VolumeId')


echo "[*] Describing volume of $INSTANCE_NAME instance (VolumeId: $volumeId)"
aws ec2 describe-volumes \
  --volume-ids  $volumeId \
  | grep -E "Device|Size|VolumeType"


VOLUME_NAME='My-Volumen'
echo "[+] Creating a new volume (Name: $VOLUME_NAME)"

newVolumeId=$(aws ec2 describe-volumes \
                --filters "Name=availability-zone,Values=$az"\
                          "Name=tag:Name,Values=$VOLUME_NAME"\
                --query 'Volumes[].VolumeId' \
                --output text)

if [[ -z $newVolumeId ]];then
  echo "[+] Creating $VOLUME_NAME volume"
  createNewVolume=$(aws ec2 create-volume \
                  --availability-zone $az \
                  --size 1 \
                  --volume-type gp2 \
                  --tag-specifications "ResourceType=volume,Tags=[{Key=Name,Value=$VOLUME_NAME}]" \
                  --output json)
  newVolumeId=$(echo $createNewVolume | jq -r '.VolumeId')
fi

echo "[*] Describing created volume (VolumeId: $newVolumeId)"
aws ec2 describe-volumes \
  --volume-ids $newVolumeId \
  | grep -E "CreateTime|Size|State|VolumeType|Encrypted"