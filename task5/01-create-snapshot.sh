#!/bin/bash
#
# This script was written to automate the following subtask
# - "Task 5: Create an Amazon EBS Snapshot"
# of Module 7 - Storage (AWS Cloud Foundations)
#
# Requirements:
# - AWS CLI


SNAPSHOT_NAME='My-Snapshot'
VOLUME_NAME='My-Volumen'

# getting ID of new volume
volumeId=$(aws ec2 describe-volumes \
              --filter "Name=tag:Name,Values=$VOLUME_NAME" \
              --query 'Volumes[].VolumeId' \
              --output text)

echo "[+] Creating an snapshot of $VOLUME_NAME volume (VolumeId: $volumeId)"
snapshotId=$(aws ec2 create-snapshot \
              --volume-id $volumeId  \
              --tag-specifications "ResourceType=snapshot,Tags=[{Key=Name,Value=$SNAPSHOT_NAME}]" \
              --query 'SnapshotId' \
              --output text)

echo "[*] Describing created snapshot (SnapshotId: $snapshotId)"
aws ec2 describe-snapshots \
  --snapshot-ids $snapshotId \
  | grep -E "VolumeId|SnapshotId|State|VolumeSize|Progress|Encrypted"

cmd="aws ec2 describe-snapshots --snapshot-ids $snapshotId | grep -E 'State|Progress'"
echo -e "[*] AWS Command to check state of snapshot:\n\t$cmd" 
