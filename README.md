## Lab 4: Working with EBS

Module: Module 7 - Storage (AWS Cloud Foundations)



Lab Overview:

![Lab 4: Working with EBS - Infrastructure](./images/infrastructure.png)

This lab focuses on Amazon Elastic Block Store (Amazon EBS), a key underlying storage mechanism for Amazon EC2 instances. In this lab, you will learn how to create an Amazon EBS volume, attach it to an instance, apply a file system to the volume, and then take a snapshot backup.