#!/bin/bash
#
# This script was written to automate the following subtask
# - "Task 2: Attach the Volume to an Instance"
# of Module 7 - Storage (AWS Cloud Foundations)
#
# Requirements:
# - AWS CLI
# - jq (JSON processor)

if [[ $# -lt 1 ]];then
  echo "USAGE: $0 BLOCK_DEVICE_NAME"
  echo -e "EXAMPLE:\n\tbash $0 /dev/xvdb"
  exit 1
fi

BLOCK_DEVICE_NAME=$1
INSTANCE_NAME=Lab
VOLUME_NAME='My-Volumen'

# getting ID of Lab instance
instanceId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[].Instances[].InstanceId' \
              --output text)

# getting ID of new volume
volumeId=$(aws ec2 describe-volumes \
              --filter "Name=tag:Name,Values=$VOLUME_NAME" \
              --query 'Volumes[].VolumeId' \
              --output text)

aws ec2 attach-volume \
  --device $BLOCK_DEVICE_NAME \
  --volume-id $volumeId \
  --instance-id  $instanceId

echo "[*] Listing attached volumes to $INSTANCE_NAME instance"
aws ec2 describe-instances \
  --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
            "Name=instance-state-name,Values=running" \
  --query 'Reservations[].Instances[].BlockDeviceMappings' \
  --no-paginate --no-cli-pager \
  --output json