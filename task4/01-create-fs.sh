#!/bin/bash
#
# This script was written to automate the following subtask
# - "Task 4: Create and Configure Your File System"
# of Module 7 - Storage (AWS Cloud Foundations)
#
# Requirements:
# - AWS CLI
#
# NOTE: 
# - This script must be run with root privileges
# - This script must be run on Lab EC2 instance, 
#   so you can send it via HTTP or using Secure Copy protocol
#
# - HTTP protocol
# |- mkdir www && cp task4/01-create-fs.sh www
# |- python3 -m http.server -d www 8080
# |- (EC2 instance) curl http://my-public-ip:8080/01-create-fs.sh
# |
# - Secure Copy protocol
# |- scp -i vockey.pem task3/01-create-fs.sh ec2-user@instance-public-ip:/tmp
# |- (EC2 instance) bash /tmp/01-create-fs.sh


if [[ $# -lt 1 ]];then
  echo "USAGE: $0 BLOCK_DEVICE_NAME"
  echo -e "EXAMPLES:\n\t $0 /dev/xvdb"
  exit 1
fi

BLOCK_DEVICE_PATH=$1
BLOCK_DEVICE_NAME=$(echo $BLOCK_DEVICE_PATH | rev | cut -d'/' -f1 | rev)
MOUNT_POINT="/mnt/data-store"

echo "[*] Checking state of $BLOCK_DEVICE_PATH block device"
sudo lsblk | grep $BLOCK_DEVICE_NAME

echo "[*] Creating ext3 file system in $BLOCK_DEVICE_PATH"
sudo mkfs -t ext3 $BLOCK_DEVICE_PATH

echo "[*] Mounting $BLOCK_DEVICE_PATH on $MOUNT_POINT"
sudo mkdir -p $MOUNT_POINT
sudo mount $BLOCK_DEVICE_PATH $MOUNT_POINT


found=$(grep "$BLOCK_DEVICE_PATH   $MOUNT_POINT ext3 defaults,noatime 1 2" /etc/fstab)
if [[ ! $found ]];then
  echo "[*] Adding $BLOCK_DEVICE_PATH entry to /etc/fstab"
  echo "$BLOCK_DEVICE_PATH   $MOUNT_POINT ext3 defaults,noatime 1 2" | sudo tee -a /etc/fstab
fi

echo "[*] Checking state of $BLOCK_DEVICE_PATH block device"
sudo lsblk | grep $BLOCK_DEVICE_NAME

FILE_PATH="/mnt/data-store/file.txt"
echo "[+] Initializing $FILE_PATH file with:"
echo "echo some text has been written" | tee $FILE_PATH